<?php
namespace LoicPennamen\FrontEditor\Twig;

use LoicPennamen\FrontEditor\FrontEditorService;
use Twig\Environment;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class FrontEditorExtension extends AbstractExtension
{
	private $sfeService;

	public function __construct(FrontEditorService $sfeService)
	{
		$this->sfeService = $sfeService;
	}

	public function getFunctions()
	{
		return [
			new TwigFunction('sfe', [$this, 'getFrontHtml'], ['is_safe' => ['html'], 'needs_environment' => true, 'needs_context' => true]),
		];
	}

	public function getFrontHtml(Environment $environment, $twigContext, string $slug, string $locale = null)
	{
		if(!$locale)
			$locale = $environment->getGlobals()['app']->getRequest()->getLocale();

		return $this->sfeService->getFrontHtml($slug, $locale, $twigContext);
	}
}
