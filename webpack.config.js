'use strict';

const CopyPlugin = require("copy-webpack-plugin");
const path = require( 'path' );
// const { styles } = require( '@ckeditor/ckeditor5-dev-utils' );
const webpack = require('webpack');

module.exports = {
	mode: 'development',
	watch: true,
	
	// https://webpack.js.org/configuration/entry-context/
	entry: './assets/js/app.js',

	// https://webpack.js.org/configuration/output/
	output: {
		path: path.resolve( __dirname, '' ),
		filename: 'sf-front-editor.js'
	},
	
	plugins: [
		new webpack.optimize.LimitChunkCountPlugin({
			maxChunks: 1,
		}),
		
		// Stable ckeditor for "/public" copy
		new CopyPlugin({
			patterns: [
				{ from: "./node_modules/ckeditor4/lang", to: "ckeditor4/lang" },
				{ from: "./node_modules/ckeditor4/plugins", to: "ckeditor4/plugins" },
				{ from: "./node_modules/ckeditor4/skins", to: "ckeditor4/skins" },
				{ from: "./node_modules/ckeditor4/vendor", to: "ckeditor4/vendor" },
				{ from: "./node_modules/ckeditor4/bower.json", to: "ckeditor4/bower.json" },
				{ from: "./node_modules/ckeditor4/ckeditor.js", to: "ckeditor4/ckeditor.js" },
				{ from: "./node_modules/ckeditor4/config.js", to: "ckeditor4/config.js" },
				{ from: "./node_modules/ckeditor4/contents.css", to: "ckeditor4/contents.css" },
				{ from: "./node_modules/ckeditor4/styles.js", to: "ckeditor4/styles.js" },
			],
		}),
	],
	
	module: {
		rules: [
			{
				test: /\.svg$/i,
				use: [ 'raw-loader' ]
			},
			{
				test: /ckeditor5-[^/\\]+[/\\]theme[/\\].+\.css$/,
				use: [
					{
						loader: 'style-loader',
						options: {
							injectType: 'singletonStyleTag',
							attributes: {
								'data-cke': true
							}
						}
					},
					{
						loader: 'postcss-loader',
						// options: styles.getPostCssConfig( {
						// 	themeImporter: {
						// 		themePath: require.resolve( '@ckeditor/ckeditor5-theme-lark' )
						// 	},
						// 	minify: true
						// } )
					}
				]
			},
			{
				test: /\.s[ac]ss$/i,
				use: [
					// Creates `style` nodes from JS strings
					'style-loader',
					// Translates CSS into CommonJS
					'css-loader',
					// Compiles Sass to CSS
					'sass-loader',
				],
			},
		]
	},

	// Useful for debugging.
	devtool: 'source-map',

	// By default webpack logs warnings if the bundle is bigger than 200kb.
	performance: { hints: false }
};
