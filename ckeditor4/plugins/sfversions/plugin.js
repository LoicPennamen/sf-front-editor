﻿CKEDITOR.plugins.add( 'sfversions', {
	icons: 'sfversions',
	init: function( editor ) {
		
		editor.addCommand( 'sfVersions', new CKEDITOR.dialogCommand( 'versionsDialog' ));
		
		CKEDITOR.dialog.add( 'versionsDialog', this.path + 'dialogs/versions.js' );

		editor.ui.addButton( 'SfVersions', {
			label: 'Charger une version précédente',
			command: 'sfVersions',
			toolbar: 'document'
		});
	}
});
