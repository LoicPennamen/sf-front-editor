CKEDITOR.dialog.add( 'versionsDialog', function ( editor ) {
	
	// Listing versions
	let elements = [];
	for (let i = 0; i < editor.sfe.versions.length; i++) {
		let file = editor.sfe.versions[i].file;
		let date = new Date(editor.sfe.versions[i].timestamp * 1000);
		let dateStr = ('0'+date.getDate()).slice(-2)
			+"/"+('0'+(date.getMonth()+1)).slice(-2)
			+"/"+date.getFullYear()
			+' à '+('0'+date.getHours()).slice(-2)
			+':'+('0'+date.getMinutes()).slice(-2)
		;
		elements.push({
			type: 'button',
			style: 'display: block; ',
			label: (i ? 'Version du ' : 'Version actuelle du ') + dateStr,
			onClick: function(e) {
				editor.sfe.loadFile(editor, file);
			}
		});
	}
	
	return {
		title: 'Versions précédentes',
		minWidth: 400,
		minHeight: 200,
		contents: [{
			id: 'tab-basic',
			label: 'Basic Settings',
			elements: elements,
		}],
		onShow: function() {
			this.disableButton('ok');
		}
	};
});
