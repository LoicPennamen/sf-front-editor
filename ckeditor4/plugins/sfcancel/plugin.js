﻿CKEDITOR.plugins.add( 'sfcancel', {
	icons: 'sfcancel',
	init: function( editor ) {
		
		editor.addCommand( 'sfCancel', {
			exec: function( editor ) {
				editor.sfe.cancel(editor);
			}
		});
		
		editor.ui.addButton( 'SfCancel', {
			label: 'Annuler les modifications',
			command: 'sfCancel',
			toolbar: 'document'
		});
	}
});
