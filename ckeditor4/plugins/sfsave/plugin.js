﻿CKEDITOR.plugins.add( 'sfsave', {
	icons: 'sfsave',
	init: function( editor ) {
		
		editor.addCommand( 'sfSave', {
			exec: function( editor ) {
				editor.sfe.save(editor);
			}
		});
		
		editor.ui.addButton( 'SfSave', {
			label: 'Enregistrer cet élément',
			command: 'sfSave',
			toolbar: 'document'
		});
	}
});
