# Symfony Front Editor #

This is an "in-page" content editor for Symfony 4+ applications. This package aims to provide 
user with a fast RTE editor for HTML blocks, thus improving content integration and webpage design.

The RTE is accessible by users logged in with specific rights. 
These users can update contents with a WYSIWYG editor on the fly.

Contents are then saved in hard files, under the `/var` directory, and backups are generated 
with an automatic rotation (ie. "old" contents are automatically deleted).

Since HTML contents are stored under the `/var` folder, 
editions can by default be tracked on GIT. Filenames contain a timestamp, 
so contents edited in production **will not** overwrite versioned content, and can also
be versioned on their end.


### Installation ###

* Add to your project using composer:  
    ```
    composer require loicpennamen/sf-front-editor
    ```

* Include required assets in your app's javascript `app.js`:
    ```
    require('../../vendor/loicpennamen/sf-front-editor/sf-front-editor');
    ``` 

* A stable version of CKEditor is included in the bundle and needs to be copied under the `public` folder. 
  Add these to your `webpack.config`:
    ```
    module.exports = {
        // (...)
  
        plugins: [          
            // (...)
		
            new CopyPlugin({
                patterns: [
                    { from: "./vendor/loicpennamen/sf-front-editor/assets/required", to: "assets/skins" },
                ],
            }),
  
        ]
    }
    ```
  
* Add routes in `/config/routes.yaml`:
    ```
    sfe:
      resource: '../vendor/loicpennamen/sf-front-editor/Controller/'
      type: annotation
    ```
  
* Add Twig templates folder ``/config/packages/twig.yaml``:
    ```
    twig:
      paths:
        'vendor/loicpennamen/sf-front-editor/templates': FrontEditorBundle
    ```
  
* Provide some users with ROLE_FRONT_EDITOR to allow for modifying any content. Example in ``security.yml`` :
    ```
    role_hierarchy:
      ROLE_ADMIN: ROLE_FRONT_EDITOR
    ```  
  
* To display an editable content block in Twig template, use this function: 
    ```
    {{ sfe('my_block_slug') }}
    ```  
  The value of `my_block_slug` can be any string containing lowercase letters, numbers, lines (-) and dash (_). It is a unique identifyer for the content, shared for every available language.
  Ie. you **do not** need to create a `slug_en` and a `slug_es`, the package will handle that part.
  

* Current language is used by default. To specify another language, pass the locale as second parameter : 
    ```
    {{ sfe('my-block-slug', 'fr') }}
    ``` 
  
* Styles can be overriden in CSS with class prefix `sfe-` :
    * sfe-front-button : Button opening the edition modale
    * sfe-content-wrapper : HTML wrapper around the content in front-end
    * sfe-content-wrapper-editable : HTML wrapper when edition is allowed
    * sfe-toolbar : Appears on top on content, when user is allowed to edit

### TO-DOs ###

* Add file manager
* Allow custom config
* Translations
* Add file metas (Author) and display in history
* Handle Ctrl-S
* Simplify installation by adding SF recipes
* Rename SF...plugins to SFE...
* Debug dlete "Remove video" button in app.js

### Development ###

During develoment, install assets with `npm install`
then watch with `node_modules\.bin\webpack.cmd`.

### Who do I talk to? ###

Loïc Pennamen  
http://loicpennamen.com
