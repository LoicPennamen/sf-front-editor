<?php

namespace LoicPennamen\FrontEditor;

use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Security;
use Twig\Environment;

class FrontEditorService
{
	private $contentFilesPath;
	private $twigEnvironment;
	private $security;
	private $router;
	private $fileExt = '.html.twig';
	private $slugPattern = "/^[a-zA-Z0-9-_]+$/";
	private $localePattern = "/^[a-z]{2}$/";
	private $maxVersions = 10;

	public function __construct(KernelInterface $kernel, Environment $twigEnvironment, Security $security, RouterInterface $router)
	{
		$this->contentFilesPath = $kernel->getProjectDir() . '/var/sfe-content';
		$this->twigEnvironment = $twigEnvironment;
		$this->security = $security;
		$this->router = $router;
	}

	/**
	 * @param string $slug
	 * @param string $locale
	 * @return string
	 * @throws \Exception
	 */
	public function getFrontHtml(string $slug, string $locale, $twigContext)
	{
		$this->validateSlug($slug);
		$htmlClasses = ['sfe-content-wrapper'];
		$htmlData = '';
		$toolBar = null;

		// Get last content file for language
		$file = $this->getLastFileBySlug($slug, $locale);

		// Not found
		if(!$file){
			$htmlContent = '';
		}

		// Twig content
		else{
			$twigContent = file_get_contents($this->getFolder($locale) . $file);
			$twigTemplate = $this->twigEnvironment->createTemplate($twigContent);
			$htmlContent = $twigTemplate->render($twigContext);
		}

		// Allow edition?
		if($this->security->isGranted("ROLE_FRONT_EDITOR")){
			$htmlClasses[] = 'sfe-content-wrapper-editable';
			$htmlData .= 'data-locale="'.$locale.'" ';
			$htmlData .= 'data-slug="'.$slug.'" ';
			$htmlData .= 'data-apiurl="'.$this->router->generate('sfe_api', [], UrlGeneratorInterface::ABSOLUTE_URL).'" ';
			$htmlData .= 'data-updateurl="'.$this->router->generate('sfe_update', ['slug' => $slug, 'locale' => $locale], UrlGeneratorInterface::ABSOLUTE_URL).'" ';
			$toolBar = $this->getHtmlToolbar([
				'slug' => $slug,
				'locale' => $locale,
			]);
		}

		return '<div class="' . implode(' ', $htmlClasses) . '" '.$htmlData.'>' . $toolBar . '<div class="sfe-content">' . $htmlContent . '</div></div>';
	}

	/**
	 * @param string $slug
	 * @param string $locale
	 * @return false|string
	 */
	public function getFrontRawContent(string $slug, string $locale)
	{
		// Get last content file for language
		$file = $this->getLastFileBySlug($slug, $locale);

		// Not found
		if(!$file)
			return '';

		return $this->getFileContent($locale, $file);
	}

	/**
	 * @param $locale
	 * @param $filename
	 * @return false|string
	 */
	public function getFileContent($locale, $filename)
	{
		return file_get_contents($this->getFolder($locale) . $filename);
	}

	/**
	 * @return string
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	private function getHtmlToolbar(array $context)
	{
		$twigContent = file_get_contents(__DIR__ . '/templates/Utils/toolbar.html.twig');
		$twigTemplate = $this->twigEnvironment->createTemplate($twigContent);
		return $this->twigEnvironment->render($twigTemplate, $context);
	}

	/**
	 * @param $slug
	 * @throws \Exception
	 */
	private function validateSlug($slug)
	{
		if(1 !== preg_match($this->slugPattern, $slug))
			throw new \Exception("Invalid FrontEditor slug pattern « $slug ». Required pattern is : ".$this->slugPattern);
	}

	/**
	 * @param $locale
	 * @throws \Exception
	 */
	private function validateLocale($locale)
	{
		if(1 !== preg_match($this->localePattern, $locale))
			throw new \Exception("Invalid FrontEditor locale pattern « $locale ». Required pattern is : ".$this->localePattern);
	}

	/**
	 * @param $slug
	 * @param $locale
	 * @return mixed
	 */
	private function getLastFileBySlug($slug, $locale)
	{
		$locale = $this->formateLocale($locale);
		$list = $this->getListBySlug($slug, $locale);

		return array_pop($list);
	}

	/**
	 * Convert a locale to lowercase, two characters string
	 *
	 * @param $locale
	 * @return false|string
	 */
	private function formateLocale($locale)
	{
		return substr(
			mb_strtolower(
				trim($locale)
			), 0, 2
		);
	}

	/**
	 * Return content files for slug, sorted by name
	 *
	 * @param $slug
	 * @param $locale
	 * @return array
	 */
	private function getListBySlug($slug, $locale)
	{
		$list = [];
		$folder = $this->getFolder($locale);

		// Check dir
		if(file_exists($folder)){
			if ($handle = opendir($folder)) {
				while (false !== ($entry = readdir($handle))) {

					// Ignore files
					if(true === in_array($entry, ['.', '..', '.htaccess']))
						continue;

					// Check extension
					if($this->fileExt !== substr($entry, strlen($this->fileExt) *-1))
						continue;

					// Check slug
					if($slug !== substr($entry, 0, strlen($slug)))
						continue;

					// Check full file name
					if(preg_match("/^" . $slug . "-[0-9]+" . $this->fileExt . "$/", $entry))
						$list[] = $entry;
				}
				closedir($handle);
			}
		}

		sort($list, SORT_STRING);

		return $list;
	}

	/**
	 * @param $slug
	 * @param $locale
	 * @return array
	 */
	public function getVersionsJson($slug, $locale)
	{
		$data = [];
		$list = $this->getListBySlug($slug, $locale);
		rsort($list);

		foreach($list as $file){
			$filePath = $this->getFolder($locale) . $file;
			$tsp = filemtime($filePath);

			$data[] = [
				'file' => $file,
				'timestamp' => $tsp,
			];
		}

		return $data;
	}

	/**
	 * @param $slug
	 * @param $locale
	 * @param $content
	 * @return array
	 * @throws \Exception
	 */
	public function saveNewContent($slug, $locale, $content)
	{
		$this->validateSlug($slug);

		// Make filename
		$fileName = $this->getFolder($locale) . $slug . '-' . date("YmdHis") . $this->fileExt;
		file_put_contents($fileName, $content);

		// Delete obsolete contents
		$this->deleteObsoleteContents($slug, $locale);

		return [
			'content' => $content,
			'slug' => $slug,
			'locale' => $locale,
			'fileName' => basename($fileName),
		];
	}

	/**
	 * @param $locale
	 * @return string
	 */
	private function getFolder($locale)
	{
		// Check locale format
		$this->validateLocale($locale);

		// Make directories
		if(true !== file_exists($this->contentFilesPath))
			mkdir($this->contentFilesPath);
		if(true !== file_exists($this->contentFilesPath . '/' . $locale))
			mkdir($this->contentFilesPath . '/' . $locale);

		return $this->contentFilesPath . '/' . $locale . '/';
	}

	/**
	 * @param $slug
	 * @param $locale
	 */
	private function deleteObsoleteContents($slug, $locale)
	{
		$files = $this->getListBySlug($slug, $locale);
		if(count($files) <= $this->maxVersions)
			return;

		// Most recent files first
		rsort($files);
		$folder = $this->getFolder($locale);
		for ($i = $this->maxVersions; $i < sizeof($files); $i++) {
			unlink($folder . $files[$i]);
		}
	}
}
