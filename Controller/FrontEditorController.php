<?php

namespace LoicPennamen\FrontEditor\Controller;

use LoicPennamen\FrontEditor\FrontEditorService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FrontEditorController extends AbstractController
{
	/**
     * @Route("/sfe/", name="sfe_api")
     */
    public function index(Request $request)
    {
    	$this->denyAccessUnlessGranted('ROLE_FRONT_EDITOR');
    	return new Response("SFE works!", 200, [
    		'X-Robots-Tag' => 'noindex'
		]);
    }

	/**
     * @Route("/sfe/update/{slug}/{locale}", name="sfe_update", requirements={"slug"="[a-zA-Z0-9-_]+", "locale"="[a-z]{2}"})
     */
    public function update(Request $request, $slug, $locale)
    {
    	$this->denyAccessUnlessGranted('ROLE_FRONT_EDITOR');

    	$frontEditorService = $this->get('LoicPennamen\FrontEditor\FrontEditorService');
		$content = $request->get('content');

    	return $this->json(
    		$frontEditorService->saveNewContent($slug, $locale, $content)
		);
    }

	/**
     * @Route("/sfe/get-file/{locale}/{filename}", name="sfe_get_file")
     */
    public function getFile(Request $request, $locale, $filename)
    {
    	$this->denyAccessUnlessGranted('ROLE_FRONT_EDITOR');

    	$frontEditorService = $this->get('LoicPennamen\FrontEditor\FrontEditorService');

    	return new Response($frontEditorService->getFileContent($locale, $filename), 200, [
			'X-Robots-Tag' => 'noindex'
		]);
    }

	/**
     * @Route("/sfe/get-versions/{locale}/{slug}", name="sfe_get_versions")
     */
    public function getVersions(Request $request, $locale, $slug)
    {
    	$this->denyAccessUnlessGranted('ROLE_FRONT_EDITOR');

    	$frontEditorService = $this->get('LoicPennamen\FrontEditor\FrontEditorService');

    	return $this->json(
			$frontEditorService->getVersionsJson($slug, $locale)
		);
    }

}
