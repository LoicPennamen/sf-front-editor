import ClassicEditor from "@ckeditor/ckeditor5-editor-classic/src/classiceditor";

// Load plugins
import Alignment from "@ckeditor/ckeditor5-alignment/src/alignment";
import Autoformat from "@ckeditor/ckeditor5-autoformat/src/autoformat";
import Base64UploadAdapter from "@ckeditor/ckeditor5-upload/src/adapters/base64uploadadapter";
import BlockQuote from "@ckeditor/ckeditor5-block-quote/src/blockquote";
import Bold from "@ckeditor/ckeditor5-basic-styles/src/bold";
import CKFinder from "@ckeditor/ckeditor5-ckfinder/src/ckfinder";
import Code from "@ckeditor/ckeditor5-basic-styles/src/code";
import CodeBlock from "@ckeditor/ckeditor5-code-block/src/codeblock";
import Essentials from "@ckeditor/ckeditor5-essentials/src/essentials";
import FontBackgroundColor from "@ckeditor/ckeditor5-font/src/fontbackgroundcolor";
import FontColor from "@ckeditor/ckeditor5-font/src/fontcolor";
import FontFamily from "@ckeditor/ckeditor5-font/src/fontfamily";
import FontSize from "@ckeditor/ckeditor5-font/src/fontsize";
import Heading from "@ckeditor/ckeditor5-heading/src/heading";
import Highlight from "@ckeditor/ckeditor5-highlight/src/highlight";
import HorizontalLine from "@ckeditor/ckeditor5-horizontal-line/src/horizontalline";
import Image from "@ckeditor/ckeditor5-image/src/image";
import ImageCaption from "@ckeditor/ckeditor5-image/src/imagecaption";
import ImageResize from "@ckeditor/ckeditor5-image/src/imageresize";
import ImageStyle from "@ckeditor/ckeditor5-image/src/imagestyle";
import ImageToolbar from "@ckeditor/ckeditor5-image/src/imagetoolbar";
import ImageUpload from "@ckeditor/ckeditor5-image/src/imageupload";
import Indent from "@ckeditor/ckeditor5-indent/src/indent";
import IndentBlock from "@ckeditor/ckeditor5-indent/src/indentblock";
import Italic from "@ckeditor/ckeditor5-basic-styles/src/italic";
import Link from "@ckeditor/ckeditor5-link/src/link";
import List from "@ckeditor/ckeditor5-list/src/list";
import MediaEmbed from "@ckeditor/ckeditor5-media-embed/src/mediaembed";
import MediaEmbedToolbar from "@ckeditor/ckeditor5-media-embed/src/mediaembedtoolbar";
import PageBreak from "@ckeditor/ckeditor5-page-break/src/pagebreak";
import Paragraph from "@ckeditor/ckeditor5-paragraph/src/paragraph";
import PasteFromOffice from "@ckeditor/ckeditor5-paste-from-office/src/pastefromoffice";
import RemoveFormat from "@ckeditor/ckeditor5-remove-format/src/removeformat";
import SpecialCharacters from "@ckeditor/ckeditor5-special-characters/src/specialcharacters";
import SpecialCharactersArrows from "@ckeditor/ckeditor5-special-characters/src/specialcharactersarrows";
import SpecialCharactersCurrency from "@ckeditor/ckeditor5-special-characters/src/specialcharacterscurrency";
import SpecialCharactersEssentials from "@ckeditor/ckeditor5-special-characters/src/specialcharactersessentials";
import SpecialCharactersLatin from "@ckeditor/ckeditor5-special-characters/src/specialcharacterslatin";
import SpecialCharactersText from "@ckeditor/ckeditor5-special-characters/src/specialcharacterstext";
import Strikethrough from "@ckeditor/ckeditor5-basic-styles/src/strikethrough";
import Subscript from "@ckeditor/ckeditor5-basic-styles/src/subscript";
import Superscript from "@ckeditor/ckeditor5-basic-styles/src/superscript";
import Table from "@ckeditor/ckeditor5-table/src/table";
import TableCellProperties from "@ckeditor/ckeditor5-table/src/tablecellproperties";
import TableProperties from "@ckeditor/ckeditor5-table/src/tableproperties";
import TableToolbar from "@ckeditor/ckeditor5-table/src/tabletoolbar";
import TextTransformation from "@ckeditor/ckeditor5-typing/src/texttransformation";
import Underline from "@ckeditor/ckeditor5-basic-styles/src/underline";

// Added plugins
import SfeSave from "../plugins/SfeSave";
import SfeCancel from "../plugins/SfeCancel";

require('../scss/front-editor.scss');

// Plugins to include in the build.
class Editor extends ClassicEditor {}
Editor.builtinPlugins = [
	Alignment,
	Autoformat,
	Base64UploadAdapter,
	BlockQuote,
	Bold,
	CKFinder,
	Code,
	CodeBlock,
	Essentials,
	FontBackgroundColor,
	FontColor,
	FontFamily,
	FontSize,
	Heading,
	Highlight,
	HorizontalLine,
	Image,
	ImageCaption,
	ImageResize,
	ImageStyle,
	ImageToolbar,
	ImageUpload,
	Indent,
	IndentBlock,
	Italic,
	Link,
	List,
	MediaEmbed,
	MediaEmbedToolbar,
	PageBreak,
	Paragraph,
	PasteFromOffice,
	RemoveFormat,
	SpecialCharacters,
	SpecialCharactersArrows,
	SpecialCharactersCurrency,
	SpecialCharactersEssentials,
	SpecialCharactersLatin,
	SpecialCharactersText,
	Strikethrough,
	Subscript,
	Superscript,
	Table,
	TableCellProperties,
	TableProperties,
	TableToolbar,
	TextTransformation,
	Underline,
	SfeSave,
	SfeCancel
];


// /////////
// Main
const appModule = {

	'init': function(){

		// Test :
		// appModule.startEditing(document.querySelector('.sfe-content-wrapper'));

		// Button listener
		document.querySelectorAll('.sfe-content-wrapper [data-action=startEditing]').forEach(button => {
			button.addEventListener('click', event => {
				let wrapper = button.closest('.sfe-content-wrapper');
				appModule.startEditing(
					wrapper
				);
			})
		});

	},

	'startEditing': function(wrapper){
		if(!wrapper)
			return console.error('SFE: Wrapper not found');

		// Set updating on
		wrapper.classList.add('sfe-editing');

		let content = wrapper.children[1];
		Editor
			.create( content, {
				heading: {
					options: [
						{ model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
						{ model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' },
						{ model: 'heading3', view: 'h3', title: 'Heading 3', class: 'ck-heading_heading3' },
						{ model: 'heading4', view: 'h4', title: 'Heading 4', class: 'ck-heading_heading4' },
						{ model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
					]
				},
				toolbar: [
					'sfeSave',
					'sfeCancel',
					'|',
					'heading',
					'|',
					'bold',
					'italic',
					'link',
					'bulletedList',
					'numberedList',
					'|',
					'alignment',
					'indent',
					'outdent',
					'code',
					'codeBlock',
					'|',
					'fontFamily',
					'fontSize',
					'fontColor',
					'fontBackgroundColor',
					'highlight',
					'|',
					'imageUpload',
					'blockQuote',
					'insertTable',
					'mediaEmbed',
					'|',
					'horizontalLine',
					'strikethrough',
					'specialCharacters',
					'subscript',
					'superscript',
					'underline',
					'removeFormat',
					'|',
					'undo',
					'redo'
				],
				allowedContent: true

			} )
			.then( editor => {

				// Focus
				editor.editing.view.focus();

				// Add custom variables
				editor.sfe = appModule;
				editor.sfe.wrapper = wrapper;
				editor.sfe.apiurl = wrapper.dataset.apiurl;
				editor.sfe.updateurl = wrapper.dataset.updateurl;
				editor.sfe.originalData = editor.getData();
				editor.sfe.contentChanged = false;

				// Debugging tool
				// CKEditorInspector.attach( editor );

				// Change listener
				editor.model.document.on( 'change:data', () => {

					// Once
					if(true === editor.sfe.contentChanged)
						return;

					// Mark content as updated
					editor.sfe.contentChanged = true;

					// Enable save button
					// TODO
				} );

				// Add spinner
				editor.sfe.spinner = appModule.addSpinner(wrapper);

			} )
			.catch( error => {
				console.error( 'CKEditor error:', error.stack );
			} )
		;
	},

	'addSpinner': function(wrapper){
		var spinnerContainer = document.createElement("DIV");
		spinnerContainer.classList.add('sfe-spinner-container');
		wrapper.appendChild(spinnerContainer);
		return spinnerContainer;
	},

	'save': function (editor) {

		// Spinner
		editor.sfe.spinner.classList.add('visible');

		// Ajax
		let xhttp = new XMLHttpRequest();

		xhttp.onreadystatechange = function() {
			if (this.readyState === 4) {
				editor.sfe.spinner.classList.remove('visible');

				// Success
				if (this.status === 200)
					appModule.saveSuccessful(editor, this.responseText);
				// Failure
				else
					appModule.saveError(xhttp);

			}
		};

		xhttp.open("POST", editor.sfe.updateurl, true);
		xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xhttp.send("content=" + encodeURIComponent(editor.getData()));
	},

	'cancel': function (editor) {

		// Check editing content
		if(editor.sfe.contentChanged){
			if(true !== confirm("The content was changed, do you really want to lose your modifications?"))
				return;
		}

		// Reset
		editor.setData(editor.sfe.originalData);

		// Edit off
		appModule.disableEditor(editor);
	},

	'saveError': function(xhttp){
		alert("Error while saving, see console for request info");
		console.log("SFE error:", xhttp);
	},

	'saveSuccessful': function(editor, responseText){

		// Edit off
		appModule.disableEditor(editor);
	},

	'disableEditor': function(editor){

		// Remove wrapper edition mode
		editor.sfe.wrapper.classList.remove('sfe-editing');

		// Remove spinner layer
		editor.sfe.spinner.remove();

		// Destroy editor
		editor.destroy();
	}
};



// /////////
// Initialize on page ready
document.addEventListener('DOMContentLoaded', function(event) {
	appModule.init();
});
