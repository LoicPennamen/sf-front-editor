import './basePath.js';
import 'ckeditor4';

require('../scss/front-editor.scss');

// /////////
// Main
const appModule = {

	'init': function(){
		
		// Test :
		// appModule.startEditing(document.querySelector('.sfe-content-wrapper'));
		
		// Button listener
		document.querySelectorAll('.sfe-content-wrapper [data-action=startEditing]').forEach(button => {
			button.addEventListener('click', event => {
				let wrapper = button.closest('.sfe-content-wrapper');
				appModule.startEditing(
					wrapper
				);
			})
		});

	},

	'startEditing': function(wrapper){
		if(!wrapper)
			return console.error('SFE: Wrapper not found');

		// Set updating on
		wrapper.classList.add('sfe-editing');

		let content = wrapper.children[1];
		
		CKEDITOR.on('instanceReady', function(evt) {
			let editor = evt.editor;
			
			// Focus
			editor.focus();
			
			// Add custom variables
			editor.sfe = appModule;
			editor.sfe.wrapper = wrapper;
			editor.sfe.slug = wrapper.dataset.slug;
			editor.sfe.locale = wrapper.dataset.locale;
			editor.sfe.apiurl = wrapper.dataset.apiurl;
			editor.sfe.updateurl = wrapper.dataset.updateurl;
			editor.sfe.versions = [];
			editor.sfe.originalData = editor.getData();
			editor.sfe.contentChanged = false;
			editor.sfe.keyTimeOut = null;
			
			// Change checker
			editor.on( 'key', function( evt ) {
				clearTimeout(editor.sfe.keyTimeOut);
				editor.sfe.keyTimeOut = window.setTimeout(function() {
					appModule.updateChangedStatus(editor);
				}, 250);
			} );
			editor.on( 'click', function( evt ) {
				appModule.updateChangedStatus(editor);
			} );
			appModule.updateChangedStatus(editor);
			
			// Gets versions
			appModule.updateVersions(editor);
			
			// Add spinner
			editor.sfe.spinner = appModule.addSpinner(wrapper);
			
		});
		
		CKEDITOR.replace(content, {
			customConfig: '', // No external config file
			extraPlugins: ['videodetector','sfsave','sfcancel','sfversions'],
			toolbar: [
				{ name: 'document', items: [ 'SfSave', 'SfCancel', 'SfVersions' ] },
				{ name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'CopyFormatting', 'RemoveFormat' ] },
				{ name: 'clipboard', items: [ 'Cut', 'Copy', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
				// { name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
				{ name: 'paragraph', items: [ 'NumberedList', 'BulletedList', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
				{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
				{ name: 'insert', items: [ 'Image', 'VideoDetector', 'Table', 'HorizontalRule', 'SpecialChar', 'Iframe' ] },
				{ name: 'styles', items: [ 'Styles', 'Format'] },
				{ name: 'tools', items: [ 'Maximize', 'Preview', 'Source' ] },
			],
			allowedContent: true,
		});
	},

	'updateChangedStatus': function(editor){
		if(editor.readOnly)
			return;
		
		// Mark content as changed or not
		editor.sfe.contentChanged = editor.getData() !== editor.sfe.originalData;
		
		// Enable/disable save button
		let state = editor.sfe.contentChanged ? CKEDITOR.TRISTATE_OFF : CKEDITOR.TRISTATE_DISABLED;
		let command = editor.getCommand( 'sfSave' );
		command.setState(state);
	},
	
	'addSpinner': function(wrapper){
		var spinnerContainer = document.createElement("DIV");
		spinnerContainer.classList.add('sfe-spinner-container');
		wrapper.appendChild(spinnerContainer);
		return spinnerContainer;
	},

	'save': function (editor) {

		// Spinner
		editor.sfe.spinner.classList.add('visible');

		// Content
		let htmlContent = editor.getData();
		// TODO : Ajustements contenus : videodetector
		htmlContent = htmlContent.replace('/<input class="remove-videodetector" type="button" value="Remove video" \/>/g', '');
		
		// Ajax
		let xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if (this.readyState === 4) {
				editor.sfe.spinner.classList.remove('visible');

				// Success
				if (this.status === 200)
					appModule.saveSuccessful(editor, this.responseText);
				// Failure
				else
					appModule.saveError(xhttp);
			}
		};

		xhttp.open("POST", editor.sfe.updateurl, true);
		xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xhttp.send("content=" + encodeURIComponent(editor.getData()));
	},

	'cancel': function (editor) {

		// Check editing content
		if(editor.sfe.contentChanged){
			if(true !== confirm("The content was changed, do you really want to lose your modifications?"))
				return;
		}

		// Reset
		editor.setData(editor.sfe.originalData);

		// Edit off
		appModule.disableEditor(editor);
	},

	'saveError': function(xhttp){
		alert("Error while saving, see console for request info");
		console.log("SFE error:", xhttp);
	},

	'saveSuccessful': function(editor, responseText){

		// Edit off
		appModule.disableEditor(editor);
	},

	'disableEditor': function(editor){

		// Remove wrapper edition mode
		editor.sfe.wrapper.classList.remove('sfe-editing');

		// Remove spinner layer
		editor.sfe.spinner.remove();

		// Destroy editor
		editor.destroy();
	},
	
	'loadFile': function(editor, filename){
		
		// Check editing content
		if(editor.sfe.contentChanged){
			if(true !== confirm("The content was changed, do you really want to lose your modifications?"))
				return;
		}
		
		// Close sfversion dialog
		CKEDITOR.dialog.getCurrent().hide()
		
		// Spinner
		editor.sfe.spinner.classList.add('visible');
		
		// Ajax
		let xhttp = new XMLHttpRequest();
		xhttp.onload = function() {
			editor.sfe.spinner.classList.remove('visible');
			editor.setData(this.responseText);
			appModule.updateChangedStatus(editor);
		};
		xhttp.open("get", editor.sfe.apiurl + 'get-file/' + editor.sfe.locale + '/' + filename, true);
		xhttp.send();
	},
	
	'updateVersions': function(editor){
		
		// Ajax
		let xhttp = new XMLHttpRequest();
		xhttp.onload = function() {
			
			// Update content versions
			editor.sfe.versions = JSON.parse(this.responseText);
			
			// enable / disable versions
			let state = editor.sfe.versions.length > 0 ? CKEDITOR.TRISTATE_OFF : CKEDITOR.TRISTATE_DISABLED;
			let command = editor.getCommand( 'sfVersions' );
			command.setState(state);
		};
		xhttp.open("get", editor.sfe.apiurl + 'get-versions/' + editor.sfe.locale + '/' + editor.sfe.slug, true);
		xhttp.send();
	}
};



// /////////
// Initialize on page ready
document.addEventListener('DOMContentLoaded', function(event) {
	appModule.init();
});
